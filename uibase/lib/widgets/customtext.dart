import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  const CustomText({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.all(25),
          margin: const EdgeInsets.all(100),
          color: Colors.deepPurple.shade200,
          child: const Text(
            'DINNER',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
