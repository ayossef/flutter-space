import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({super.key});
  void favClicked() {
    print('Button has been clicked');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.deepPurple.shade600,
      margin: const EdgeInsets.all(100),
      padding: const EdgeInsets.all(25),
      child: ElevatedButton(
          onPressed: favClicked, child: const Icon(Icons.favorite_sharp)),
    );
  }
}
