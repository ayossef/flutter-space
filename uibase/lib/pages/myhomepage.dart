import 'package:flutter/material.dart';
import 'package:uibase/widgets/custombutton.dart';
import 'package:uibase/widgets/customtext.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomText(value: ''),
          CustomButton(),
          CustomButton(),
          CustomText()
        ],
      ),
    );
  }
}
