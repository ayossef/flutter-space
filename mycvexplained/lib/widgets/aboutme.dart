import 'package:flutter/material.dart';
import 'package:mycvexplained/model/listmanager.dart';

class AboutMe extends StatefulWidget {
  const AboutMe({super.key});

  @override
  State<AboutMe> createState() => _AboutMeState();
}

class _AboutMeState extends State<AboutMe> {
  List<String> certificates = [];
  TextEditingController certificateNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    certificates = ListManager.instance.cerificates;
    return Center(
      child: Column(
        children: [
          Text('AboutMe Section'),
          Container(
            margin: EdgeInsets.all(5),
            child: TextField(
              controller: certificateNameController,
              decoration: InputDecoration(hintText: 'New Certificate Name'),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                print(certificateNameController.text);
                certificates.add(certificateNameController.text);
                print(certificates);
                certificateNameController.clear();
              },
              child: Text('Submit'))
        ],
      ),
    );
  }
}
