import 'package:flutter/material.dart';
import 'package:mycvexplained/model/listmanager.dart';

class Certificates extends StatefulWidget {
  const Certificates({super.key});

  @override
  State<Certificates> createState() => _CertificatesState();
}

class _CertificatesState extends State<Certificates> {
  List<String> certList = [];
  @override
  Widget build(BuildContext context) {
    certList = ListManager.instance.cerificates;
    return ListView.builder(
      itemBuilder: (context, index) => Container(
        padding: EdgeInsets.all(3),
        margin: EdgeInsets.all(5),
        child: Text(
          certList[index],
          style: TextStyle(
            fontSize: 24,
          ),
        ),
        color: Colors.amber,
      ),
      itemCount: certList.length,
    );
  }
}
