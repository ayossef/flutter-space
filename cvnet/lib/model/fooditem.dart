class Food {
  String? id;
  String? name;
  int? cals;
  int? price;
  String? image;

  Food({this.id, this.name, this.cals, this.price, this.image});

  // Food.fromJson(Map<String, dynamic> json) {
  //   id = json['id'];
  //   name = json['name'];
  //   cals = json['cals'];
  //   price = json['price'];
  //   image = json['image'];
  // }

  Food.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    cals = json['cals'];
    price = json['price'];
    image = json['image'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['cals'] = this.cals;
    data['price'] = this.price;
    data['image'] = this.image;
    return data;
  }
}
