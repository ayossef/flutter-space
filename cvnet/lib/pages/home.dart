import 'package:cvnet/views/list.dart';
import 'package:cvnet/views/login.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Column(
        children: [
          // LoginView(),
          MyItemsList(),
        ],
      ),
    );
  }
}
