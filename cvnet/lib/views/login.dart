import 'package:flutter/material.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        Container(
          margin: EdgeInsets.all(5),
          child: TextField(
            decoration:
                InputDecoration(border: InputBorder.none, hintText: 'Username'),
          ),
        ),
        Container(
          margin: EdgeInsets.all(5),
          child: TextField(
            decoration:
                InputDecoration(border: InputBorder.none, hintText: 'password'),
          ),
        ),
        MaterialButton(
          onPressed: () {},
          child: Text('Login'),
          color: Colors.amber,
        )
      ]),
    );
  }
}
