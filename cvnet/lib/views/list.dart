import 'dart:convert';

import 'package:cvnet/model/fooditem.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class MyItemsList extends StatefulWidget {
  MyItemsList({super.key});

  @override
  State<MyItemsList> createState() => _MyItemsListState();
}

class _MyItemsListState extends State<MyItemsList> {
  List<Food> _items = [];
  bool loaded = false;
  void loadData() async {
    _items = [];
    var url = Uri.http('localhost:8000', 'food');
    var response = await http.get(url);
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');
    List<dynamic> jsonResp = json.decode(response.body);
    print(jsonResp);
    List<dynamic> output = json.decode(response.body);
    print(output.length);
    print((output[0] as Map<String, dynamic>)['name']);
    List list = [];
    print(Food.fromJson(output[0]).name);
    setState(() {
      output.forEach((element) {
        _items.add(Food.fromJson(element));
      });
    });
    print(json.encode(_items));
  }

  void navigate(int index) {
    print(index);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home')),
      body: Column(
        children: [
          Text('${_items.length}'),
          MaterialButton(
            onPressed: loadData,
            child: Icon(Icons.refresh),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _items.length,
              itemBuilder: ((context, index) => Column(
                    children: [
                      Text(_items[index].name!),
                      MaterialButton(onPressed: () => {navigate(index)})
                    ],
                  )),
            ),
          )
        ],
      ),
    );
  }
}
