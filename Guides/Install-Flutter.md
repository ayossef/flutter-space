# Installing Flutter

In order to install Flutter env. we need to have:
- Flutter SDK
- IDE (Editor - VSCode)
- Runtime Env. (Desktop Native and Chrome)
  

## Install Flutter SDK
In order to install flutter sdk, just download the sdk
https://docs.flutter.dev/get-started/install

```bash
Make sure to add sdk/bin to PATH
```

Note the following commands:

```bash
flutter doctor # Inspect the env
flutter create <project_name> 
flutter run # to run the project
```


## Install VS Code 
Just download and run the wizard
+ Adding plugin


