import 'package:certificatesexpained/views/viewone.dart';
import 'package:certificatesexpained/views/viewthree.dart';
import 'package:certificatesexpained/views/viewtwo.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  List<Widget> allViews = [
    const FirstView(),
    const SecondView(),
    const ThirdView()
  ];
  List<BottomNavigationBarItem> bnbItems = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.one_k),
      label: 'First',
    ),
    const BottomNavigationBarItem(icon: Icon(Icons.two_k), label: 'Second'),
    const BottomNavigationBarItem(icon: Icon(Icons.three_k), label: "Third")
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home Page')),
      body: allViews[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
          items: bnbItems,
          currentIndex: _selectedIndex,
          onTap: (value) {
            setState(() {
              _selectedIndex = value;
              print('Item Tapped');
              print(_selectedIndex);
            });
          }),
    );
  }
}
