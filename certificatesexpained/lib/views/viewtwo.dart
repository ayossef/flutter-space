import 'package:certificatesexpained/model/listmanager.dart';
import 'package:flutter/material.dart';

class SecondView extends StatefulWidget {
  const SecondView({super.key});

  @override
  State<SecondView> createState() => _SecondViewState();
}

class _SecondViewState extends State<SecondView> {
  TextEditingController _userInputController = TextEditingController();
  List<String> items = ListManager.items;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            child: Text('Second View'),
            padding: EdgeInsets.all(5),
          ),
          TextField(
            controller: _userInputController,
            decoration:
                InputDecoration(hintText: 'Enter the name of the new cert'),
          ),
          ElevatedButton(
              onPressed: () {
                ListManager.items.add(_userInputController.text);
                _userInputController.clear();
              },
              child: Text('Add to the list'))
        ],
      ),
    );
  }
}
