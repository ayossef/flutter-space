import 'package:flutter/material.dart';

class ThirdView extends StatelessWidget {
  const ThirdView({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            child: Text('Third View'),
            padding: EdgeInsets.all(5),
          ),
        ],
      ),
    );
  }
}
