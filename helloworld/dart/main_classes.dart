void main() {
  User user1 = User.withEmail("Ahmed", "me@email.com");
  User user2 = User.withEmailAndAge("User2", "User2@gmail.com", 20);
  user1.id = 1;
  user2.id = 2;
  user1.printUserData();
  user2.printUserData();
  print(User.MAX_AGE);
}

class User {
  String name = "";
  int? id;
  int? age;
  String? email;

  // User(String name) {
  //   this.name = name;
  // }
  static var MAX_AGE = 100;
  User(this.name);
  User.withEmail(this.name, this.email);
  User.withEmailAndAge(this.name, this.email, this.age);

  void printUserData() {
    print(
        "$id. User $name is $age years old and you can contact him on $email");
  }
}
