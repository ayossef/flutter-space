import 'dart:io';

void main() {
  //testVarAndPrinting();
  // forLoops();
  // whileAndIfElse();
  // readInputAndNullables();
}

String? getItem(List<String> mylist, int index) {
  if (index < mylist.length && index >= 0) {
    return mylist[index];
  } else {
    return null;
  }
}

void readInputAndNullables() {
  List<String> theList = [];
  var theIndex = 10;
  print(getItem(theList, theIndex));

  print("Please Enter your name");
  String? username = stdin.readLineSync(); // nullable string
  print("Hello $username");
  print("Enter your age");
  String? userInput = stdin.readLineSync();
  if (userInput != null) {
    int age = int.parse(userInput);
    print("You are $age");
  }
}

void whileAndIfElse() {
  int x = 0;
  while (x < 10) {
    print("Value of X  = $x");
    x--;
    if (x == -1) {
      print("Continue ...");
      continue;
    } else if (x == -5) {
      print("breaking ..");
      break;
    }
  }
}

void forLoops() {
  var list = [10, 20, 30, 40, 50, 60];

  List<String> countries = ["MZ", "EG", "AE", "UK", "US"];

  for (var i = 0; i < countries.length; i++) {
    var country = countries[i];
    print("Current Country is $country");
  }

  for (var country in countries) {
    print("Current Country is $country");
  }
  for (var item in list) {
    print("$item");
  }

  for (var i = 0, j = 10, k = 20; i < 10; i++, j++, k++) {
    print("Current Iteration is $i, $j, $k");
  }
}

void testVarAndPrinting() {
  // variables
  int x = 10;
  String message = "hello";
  double salary = 12.4;
  var email = "me@mail.com";
  var age = 40;

  // printing
  print(x);
  print("Message is $message");
  print("Salary is $salary\$ for user $email who is $age years old");
}
