import 'package:flutter/material.dart';
import 'package:users/model/user.dart';
import 'package:users/model/userapi.dart';

import 'basescreen.dart';

class UsersList extends StatefulWidget {
  const UsersList({Key? key}) : super(key: key);

  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Users List",
      widgets: [
        Icon(Icons.star_outline),
        FutureBuilder(
            future: UserApi().getAllUsers(),
            builder: (BuildContext context,
                AsyncSnapshot<List<User>> usersListSnapShot) {
              if (usersListSnapShot.hasData) {
                List<User>? usersList = usersListSnapShot.data;
                return Text("${usersList![0].email}");
              }
              return Text("Data is not Ready Yet");
            }),
        FutureBuilder(
          builder: (BuildContext contxt, AsyncSnapshot<User> userSnapshot) {
            if (userSnapshot.hasData) {
              User user = userSnapshot.data!;
              return Text("${user.email}");
            }
            return Text("Data is not read yet");
          },
          future: UserApi().getAUser(9),
        ),
      ],
    );
  }
}
