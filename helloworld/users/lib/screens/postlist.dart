import 'package:flutter/material.dart';
import 'package:users/screens/basescreen.dart';

class PostsList extends StatefulWidget {
  const PostsList({Key? key}) : super(key: key);

  @override
  _PostsListState createState() => _PostsListState();
}

class _PostsListState extends State<PostsList> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(title: "Posts Lists", widgets: [Text("Posts Screen")]);
  }
}
