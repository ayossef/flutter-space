import 'package:flutter/material.dart';

class BaseScreen extends StatelessWidget {
  const BaseScreen({Key? key, required this.title, required this.widgets})
      : super(key: key);

  final String title;
  final List<Widget> widgets;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${this.title}"),
      ),
      body: Container(
        child: Column(
          children: this.widgets,
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [Icon(Icons.star_rate), Text("Main Menu")],
              ),
            ),
            ListTile(
              title: Text("Show Users"),
              leading: Icon(Icons.verified_user),
              onTap: () {
                print("User Clicked");
              },
            ),
            ListTile(
              title: Text("Show Posts"),
              leading: Icon(Icons.post_add_sharp),
              onTap: () {
                print("Posts Clicked");
              },
            )
          ],
        ),
      ),
    );
  }
}
