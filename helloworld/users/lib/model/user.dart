import 'dart:convert';

class User {
  String name;
  String phone;
  String email;
  int id;

  User(
      {required this.name,
      required this.email,
      required this.id,
      required this.phone});

  setValues(Map<String, dynamic> jsonData) {
    this.name = jsonData["name"];
    this.email = jsonData["email"];
    this.phone = jsonData["phone"];
    this.id = jsonData["id"];
  }

  factory User.fromJson(Map<String, dynamic> jsonData) {
    return User(
        email: jsonData["email"],
        name: jsonData["name"],
        phone: jsonData["phone"],
        id: jsonData["id"]);
  }
}
