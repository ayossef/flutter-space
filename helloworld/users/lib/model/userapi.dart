import 'dart:convert';
import 'package:http/http.dart' as http;
import 'user.dart';

class UserApi {
  final String url = "https://jsonplaceholder.typicode.com/users";

  Future<List<User>> getAllUsers() async {
    http.Response resp = await http.get(Uri.parse(url));
    List<User> userList = [];

    if (resp.statusCode == 200) {
      List<dynamic> dataList = jsonDecode(resp.body);
      for (var jsonMap in dataList) {
        userList.add(User.fromJson(jsonMap));
      }
    }
    return userList;
  }

  Future<User> getAUser(int id) async {
    http.Response resp = await http.get(Uri.parse(url + "/" + "$id"));
    User? currentUser;
    if (resp.statusCode == 200) {
      var jsonMap = jsonDecode(resp.body);
      currentUser = User.fromJson(jsonMap);
    }
    return currentUser!;
  }
}
