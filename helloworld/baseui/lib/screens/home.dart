import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextStyle btnTxtStyle = TextStyle(fontSize: 21, color: Colors.blueGrey);
  static final int magicNum = 7;
  static final int maxTrails = 5;

  int _guessNum = 0;
  void updateGuessNum(int newValue) {
    setState(() {
      _guessNum = newValue;
    });
  }

  int _numberOfTrails = 0;
  void incNumTrials() {
    setState(() {
      _numberOfTrails++;
    });
  }

  String _result = "";
  void updateResult(String newValue) {
    setState(() {
      _result = newValue;
    });
  }

  void textChanged(String value) {
    updateGuessNum(int.parse(value));
  }

  void guessButtonAction() {
    incNumTrials();
    if (_numberOfTrails >= maxTrails) {
      updateResult("You have reached max number of trails");
      return;
    }
    String outputMsg = setGuessResult();
    updateResult(outputMsg);
  }

  String setGuessResult() {
    String outputMsg = "";
    if (_guessNum == magicNum) {
      outputMsg = "Congratulations .. :D :D";
    } else {
      outputMsg = "Try Harder ..";
    }
    return outputMsg;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.9,
        title: Text('Home Screen'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'Welcome to game',
            style: TextStyle(fontSize: 30, color: Colors.blue),
          ),
          TextField(
            onChanged: textChanged,
          ),
          MaterialButton(
            onPressed: guessButtonAction,
            child: Text(
              'Guess',
              style: btnTxtStyle,
            ),
          ),
          Text('$_result'),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: () => {},
                child: Text(
                  'Show High Scores',
                  style: btnTxtStyle,
                ),
              ),
              SizedBox(
                width: 50,
              ),
              MaterialButton(
                onPressed: () => {},
                child: Text(
                  'Start New Game',
                  style: btnTxtStyle,
                ),
              )
            ],
          )
        ],
      ),
      backgroundColor: Colors.grey.shade200,
    );
  }
}
