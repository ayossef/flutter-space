import 'package:bloc/bloc.dart';

import 'counter_data.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
class CounterCubit extends Cubit<CounterDataState> {
  /// {@macro counter_cubit}
  CounterCubit() : super(CounterDataState(userInput: "", counterValue: 0));

  /// Add 1 to the current state.
  void increment() {
    print("adding");
    emit(CounterDataState(
        userInput: state.userInput, counterValue: state.counterValue + 1));
  }

  /// Subtract 1 from the current state.
  void decrement() {
    emit(CounterDataState(
        userInput: state.userInput, counterValue: state.counterValue - 1));
  }

  void clear() {
    emit(state.getClearedState());
  }

  void addValue(int value) {
    emit(state.getAddedValue(value));
  }
}
