class CounterDataState {
  String userInput = "";
  int counterValue = 0;

  CounterDataState({required this.userInput, required this.counterValue});

  CounterDataState getClearedState() {
    return CounterDataState(userInput: this.userInput, counterValue: 0);
  }

  CounterDataState getAddedValue(int value) {
    return CounterDataState(
        userInput: this.userInput, counterValue: this.counterValue + value);
  }
}
