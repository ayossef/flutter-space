import 'dart:convert';
import 'package:appsix/model/post.dart';
import 'package:http/http.dart' as http;

class PostService {
  //final String url = "https://jsonplaceholder.typicode.com/posts";
  final String url = "http://localhost:3000/posts";

  Future<List<Post>> getAllPosts() async {
    http.Response resp = await http.get(Uri.parse(url));
    List<Post> list = [];

    if (resp.statusCode == 200) {
      List<dynamic> dataList = jsonDecode(resp.body);
      for (var jsonMap in dataList) {
        list.add(Post.fromJson(jsonMap));
      }
    }
    return list;
  }

  Future<Post> getPost(int id) async {
    http.Response resp = await http.get(Uri.parse(url + "/" + "$id"));
    Post? post;
    if (resp.statusCode == 200) {
      var jsonMap = jsonDecode(resp.body);
      post = Post.fromJson(jsonMap);
    }
    return post!;
  }
}
