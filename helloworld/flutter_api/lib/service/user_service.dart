import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:appsix/model/user.dart';

class UserService {
  //final String url = "https://jsonplaceholder.typicode.com/users";
  final String url = "http://localhost:3000/users";

  Future<List<User>> getUsers() async {
    http.Response resp = await http.get(Uri.parse(url));
    List<User> list = [];

    if (resp.statusCode == 200) {
      List<dynamic> data = jsonDecode(resp.body);
      for (var json in data) {
        list.add(User.fromJson(json));
      }
    }
    return list;
  }

  Future<User> getUser(int id) async {
    http.Response response = await http.get(Uri.parse(url + "/" + "$id"));
    User? model;
    if (response.statusCode == 200) {
      model = User.fromJson(jsonDecode(response.body));
    }
    return model!;
  }

  void addUser(User user) async {
    http.Response response =
        await http.post(Uri.parse(url), body: user.toJson());
    if (response.statusCode == 200) {}
  }

  void deleteUser(int id) async {
    print('inside deleteUser');
    await http.delete(Uri.parse(url + "/" + "$id"));
  }
}
