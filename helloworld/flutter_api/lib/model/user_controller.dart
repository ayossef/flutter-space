import 'package:appsix/model/user.dart';
import 'package:appsix/service/user_service.dart';

class UserController {
  Future<List<User>> list() {
    return UserService().getUsers();
  }

  add(User newuser) {}
  Future<User> view(int userId) {
    return UserService().getUser(userId);
  }

  delete(User userToDelete) {}
}
