import 'package:flutter/material.dart';

class Post {
  static const ID = "id";
  static const USER_ID = "userId";
  static const TITLE = "title";
  static const BODY = "body";

  final int id;
  final int userId;
  final String title;
  final String body;

  const Post({
    required this.id,
    required this.userId,
    required this.title,
    required this.body,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        userId: json[USER_ID],
        title: json[TITLE],
        body: json[BODY],
        id: json[ID]);
  }
}
