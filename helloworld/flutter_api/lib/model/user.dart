import 'package:flutter/material.dart';

class User {
  static const ID = "id";
  static const NAME = "name";
  static const EMAIL = "email";
  static const USERNAME = "username";
  static const PHONE = "phone";
  static const IMAGE = "image";

  final int id;
  final String name;
  final String email;
  final String image;
  final String username;
  final String phone;
  //final IconData icon = Icons.person;

  const User(
      {required this.id,
      required this.name,
      required this.email,
      required this.image,
      required this.username,
      required this.phone});

  factory User.fromJson(Map<String, dynamic> json) {
    var modulo = json[ID] % 10;
    if (modulo == 0) {
      modulo = 1;
    }
    return User(
        username: json[USERNAME],
        image: 'assets/avatar (${modulo}\).png',
        email: json[EMAIL],
        name: json[NAME],
        phone: json[PHONE],
        id: json[ID]);
  }

  Map<String, dynamic> toJson() {
    return {
      //ID: id.toString(),
      NAME: name,
      EMAIL: email,
      USERNAME: username,
      PHONE: phone
    };
  }
}
