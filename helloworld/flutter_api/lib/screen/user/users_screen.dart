import 'package:appsix/model/model_states.dart';
import 'package:appsix/model/user.dart';
import 'package:appsix/model/user_controller.dart';
import 'package:appsix/screen/user/user_detail_screen.dart';
import 'package:appsix/screen/user/user_registration_screen.dart';
import 'package:appsix/service/user_service.dart';
import 'package:flutter/material.dart';

import '../base_screen.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({Key? key}) : super(key: key);

  @override
  _UsersScreenState createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  late Future<List<User>> users;

  @override
  void initState() {
    super.initState();
    users = UserController().list();
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Users",
      widgets: [
        FutureBuilder(
            future: users,
            builder:
                (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
              if (snapshot.hasData) {
                List<User>? users = snapshot.data;
                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: users!.length,
                  itemBuilder: (context, index) {
                    User user = users[index];
                    return Card(
                        child: GestureDetector(
                      onTap: () {
                        _details(user, context);
                      },
                      child: _buildItem(user),
                    ));
                  },
                );
              }
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                  ],
                ),
              );
            }),
        FloatingActionButton(
          onPressed: addUser,
          tooltip: 'Add',
          child: Icon(Icons.add),
        ),
        UserState.flag
            ? Text("User ${UserState.recentName} has been added")
            : Text(""),
      ],
    );
  }

  Widget _buildItem(User user) {
    return ListTile(
      key: ValueKey<User>(user),
      title: Text(
        user.name,
        style: TextStyle(fontSize: 21),
      ),
      subtitle: Text(user.email),
      leading: CircleAvatar(backgroundImage: AssetImage(user.image)),
      trailing: IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () => {_delete(user, context)},
      ),
    );
  }

  Future<void> _delete(User user, BuildContext context) async {
    UserService().deleteUser(user.id);
    setState(() {
      users = UserService().getUsers();
    });
  }

  Future<void> _details(User user, BuildContext context) async {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => UserDetailScreen(id: user.id)));
  }

  addUser() async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                UserRegistrationScreen(parentScreen: widget)));
    setState(() {
      users = UserService().getUsers();
    });
  }

  addUser2() async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                UserRegistrationScreen(parentScreen: widget)));
    if (result != null) {
      User u = result as User;
      // setState(() {
      //   users = UserService().getUsers();
      // });
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
            SnackBar(content: Text('User \"${u.name}\" added successfully!')));
    }
  }
}
