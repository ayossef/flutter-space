import 'package:appsix/model/user.dart';
import 'package:appsix/service/user_service.dart';
import 'package:flutter/material.dart';

class UserDetailsScreen extends StatelessWidget {
  // In the constructor, require a User.
  const UserDetailsScreen({Key? key, required this.user}) : super(key: key);

  // Declare a field that holds the User.
  final User user;

  static String getFirstWord(String inputString) {
    List<String> wordList = inputString.split(" ");
    if (wordList.isNotEmpty) {
      return wordList[0];
    } else {
      return ' ';
    }
  }

  @override
  Widget build(BuildContext context) {
    // Use the User to create the UI.
    return Scaffold(
      appBar: AppBar(
        //title: Text(user.name),
        title: Text("User: " + getFirstWord(user.name)),
      ),
      body: ListView(
        children: [
          CircleAvatar(
              backgroundColor: Colors.amber,
              radius: 120,
              child: CircleAvatar(
                  radius: 110, backgroundImage: AssetImage(user.image))),
          Card(
            child: ListTile(
                title: Text(user.name),
                subtitle: Text("name"),
                leading: Icon(Icons.person),
                trailing: Icon(Icons.card_membership)),
          ),
          Card(
            child: ListTile(
                title: Text(user.email),
                subtitle: Text("email"),
                leading: Icon(Icons.email),
                trailing: Icon(Icons.alternate_email)),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          load(context);
        },
        tooltip: 'Load',
        child: Icon(Icons.arrow_downward_rounded),
      ),
    );
  }

  Future<void> load(BuildContext context) async {
    final result = await UserService().getUser(9);
    if (result != null) {}
  }
}
