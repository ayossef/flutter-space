import 'package:appsix/model/user.dart';
import 'package:appsix/service/user_service.dart';
import 'package:flutter/material.dart';

class UserDetailScreen extends StatefulWidget {
  UserDetailScreen({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  _UserDetailScreenState createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  late Future<User> futureUser;

  @override
  void initState() {
    super.initState();
    futureUser = UserService().getUser(widget.id);
  }

  static String getFirstWord(String inputString) {
    List<String> wordList = inputString.split(" ");
    if (wordList.isNotEmpty) {
      return wordList[0];
    } else {
      return ' ';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureUser,
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.hasData) {
            User? user = snapshot.data;
            return Scaffold(
              appBar: AppBar(
                title: Text("User: " + getFirstWord(user!.name)),
              ),
              body: ListView(
                children: [
                  CircleAvatar(
                      backgroundColor: Colors.amber,
                      radius: 120,
                      child: CircleAvatar(
                          radius: 110,
                          backgroundImage: AssetImage(user.image))),
                  Card(
                    child: ListTile(
                        title: Text(user.name),
                        subtitle: Text("name"),
                        leading: Icon(Icons.person),
                        trailing: Icon(Icons.card_membership)),
                  ),
                  Card(
                    child: ListTile(
                        title: Text(user.email),
                        subtitle: Text("email"),
                        leading: Icon(Icons.email),
                        trailing: Icon(Icons.alternate_email)),
                  ),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  load();
                },
                tooltip: 'Load',
                child: Icon(Icons.arrow_downward_rounded),
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        });
  }

  Future<void> load() async {
    setState(() {
      futureUser = UserService().getUser(widget.id);
    });
  }
}
