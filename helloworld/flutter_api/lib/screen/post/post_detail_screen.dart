import 'package:appsix/model/post.dart';
import 'package:appsix/service/post_service.dart';
import 'package:flutter/material.dart';

class PostDetailScreen extends StatefulWidget {
  PostDetailScreen({Key? key, required this.id}) : super(key: key);
  final int id;

  @override
  _PostDetailScreenState createState() => _PostDetailScreenState();
}

class _PostDetailScreenState extends State<PostDetailScreen> {
  late Future<Post> futurePost;

  @override
  void initState() {
    super.initState();
    futurePost = PostService().getPost(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futurePost,
        builder: (BuildContext context, AsyncSnapshot<Post> snapshot) {
          if (snapshot.hasData) {
            Post? post = snapshot.data;
            return Scaffold(
              appBar: AppBar(
                //title: Text(user.name),
                title: Text("Post Id: " + post!.id.toString()),
              ),
              body: ListView(
                children: [
                  Card(
                    child: ListTile(
                      title: Text(post.title),
                      subtitle: Text("Title"),
                      leading: Icon(Icons.title_rounded),
                    ),
                  ),
                  Card(
                    child: ListTile(
                        title: Text(post.body),
                        subtitle: Text("Body"),
                        leading: Icon(Icons.book),
                        trailing: Icon(Icons.post_add_rounded)),
                  ),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  load();
                },
                tooltip: 'Load',
                child: Icon(Icons.arrow_downward_rounded),
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                ],
              ),
            );
          }
        });
  }

  Future<void> load() async {
    setState(() {
      futurePost = PostService().getPost(widget.id);
    });
  }
}
