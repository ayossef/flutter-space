import 'package:appsix/model/post.dart';
import 'package:appsix/screen/post/post_detail_screen.dart';
import 'package:appsix/service/post_service.dart';
import 'package:flutter/material.dart';

import '../base_screen.dart';

class PostsScreen extends StatefulWidget {
  const PostsScreen({Key? key}) : super(key: key);

  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: "Posts",
      widgets: [
        FutureBuilder(
            future: PostService().getAllPosts(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Post>> snapShot) {
              if (snapShot.hasData) {
                List<Post>? list = snapShot.data;
                //return Text("${usersList![0].email}");
                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: list!.length,
                  itemBuilder: (context, index) {
                    Post model = list[index];
                    return Card(
                        child: GestureDetector(
                      onTap: () {
                        _details(model, context);
                      },
                      child: _buildItem(model),
                    ));
                  },
                );
              }
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                  ],
                ),
              );
            }),
      ],
    );
  }

  Widget _buildItem(Post model) {
    return ListTile(
      title: Text(
        model.title,
        style: TextStyle(fontSize: 21),
      ),
      subtitle: Text(model.body),
      leading: Icon(Icons.list),
      trailing: IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () => {_details(model, context)},
      ),
    );
  }

  Future<void> _details(Post model, BuildContext context) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PostDetailScreen(id: model.id)));
  }
}
