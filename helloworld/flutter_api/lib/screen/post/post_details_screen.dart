import 'package:appsix/model/post.dart';
import 'package:appsix/service/user_service.dart';
import 'package:flutter/material.dart';

class PostDetailsScreen extends StatelessWidget {
  const PostDetailsScreen({Key? key, required this.post}) : super(key: key);

  final Post post;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //title: Text(user.name),
        title: Text("Post Id: " + post.id.toString()),
      ),
      body: ListView(
        children: [
          Card(
            child: ListTile(
              title: Text(post.title),
              subtitle: Text("Title"),
              leading: Icon(Icons.title_rounded),
            ),
          ),
          Card(
            child: ListTile(
                title: Text(post.body),
                subtitle: Text("Body"),
                leading: Icon(Icons.book),
                trailing: Icon(Icons.post_add_rounded)),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          load(context);
        },
        tooltip: 'Load',
        child: Icon(Icons.arrow_downward_rounded),
      ),
    );
  }

  Future<void> load(BuildContext context) async {
    final result = await UserService().getUser(9);
    if (result != null) {
      //this.user = result;
    }
  }
}
