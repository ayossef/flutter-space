import 'package:appsix/screen/post/posts_screen.dart';
import 'package:appsix/screen/user/users_screen.dart';
import 'package:flutter/material.dart';

class BaseScreen extends StatelessWidget {
  const BaseScreen({Key? key, required this.title, required this.widgets})
      : super(key: key);

  final String title;
  final List<Widget> widgets;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${this.title}"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: this.widgets,
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              accountName: Text('Imran Issufo'),
              accountEmail: Text('imran.issufo@gmail.com'),
              currentAccountPicture: CircleAvatar(
                radius: 100,
                child: CircleAvatar(
                  child: ClipRRect(
                    child: Image.asset('person.png'),
                  ),
                ),
              ),
            ),
            ListTile(
              title: Text("Show Users"),
              leading: Icon(Icons.verified_user),
              onTap: () {
                print("User Clicked");
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UsersScreen()));
              },
            ),
            ListTile(
              title: Text("Show Posts"),
              leading: Icon(Icons.post_add_sharp),
              onTap: () {
                print("Posts Clicked");
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PostsScreen()));
              },
            )
          ],
        ),
      ),
    );
  }
}
