import 'package:flutter/material.dart';
import 'package:lists/model/lesson.dart';
import 'package:lists/screens/lessondetails.dart';

class MainList extends StatelessWidget {
  final titles = ["Day 1", "Day 2", "Day 3", "Day 4"];
  final subtitles = [
    "Tutorial: Building UI",
    "Tutorial: Guessing Number Game",
    "Tutorial: Building Lists",
    "Tutorial: Network Operations"
  ];
  final icons = [Icons.wallet_travel, Icons.access_alarm, Icons.access_time];

  @override
  Widget build(BuildContext context) {
    Lesson.loadInitLessons();
    return Scaffold(
      appBar: AppBar(
        title: Text("Main List"),
      ),
      body: Center(
        child: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (context, index) {
              print("Building cell at index $index");
              return Card(
                  child: GestureDetector(
                onTap: () {
                  print("List item at index $index was tapped");
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LessonDetails(
                              currentLesson: Lesson.lessonsList[index])));
                },
                child: ListTile(
                  title: Text(
                    Lesson.lessonsList[index].title,
                    style: TextStyle(fontSize: 21),
                  ),
                  subtitle: Text(Lesson.lessonsList[index].subtitle),
                  leading: CircleAvatar(
                      backgroundImage:
                          NetworkImage(Lesson.lessonsList[index].imgUrl)),
                  trailing: Icon(Lesson.lessonsList[index].icon),
                ),
              ));
            }),
      ),
    );
  }
}
