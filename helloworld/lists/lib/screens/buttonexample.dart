import 'package:flutter/material.dart';

class ButtonStatus extends StatefulWidget {
  const ButtonStatus({Key? key}) : super(key: key);

  @override
  _ButtonStatusState createState() => _ButtonStatusState();
}

class _ButtonStatusState extends State<ButtonStatus> {
  int counter = 0;
  void action() {
    setState(() {
      counter++;
    });
    if (counter == 5) {
      _buttonStatus = !_buttonStatus;
    }
  }

  bool _buttonStatus = false;

  Widget _buildButton() {
    return new MaterialButton(
      child: new Text(_buttonStatus ? "inactive" : "active"),
      onPressed: _buttonStatus ? null : action,
    );
  }

  bool _status = true;
  int _counter = 0;
  void buttonAction() {
    print("Button is clicked");
    setState(() {
      _counter++;
    });
    if (_counter == 5) {
      setState(() {
        _status = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button Exmaple"),
      ),
      body: Container(
        child: Column(
          children: [buildButton()],
        ),
      ),
    );
  }

  MaterialButton buildButton() {
    return MaterialButton(
      onPressed: _status ? buttonAction : null,
      child: Text(
        _status ? "Click Me" : "Do not click me",
        style: TextStyle(
            color: _status ? Colors.amber : Colors.grey, fontSize: 40),
      ),
    );
  }
}
