import 'package:flutter/material.dart';
import 'package:lists/model/lesson.dart';

class LessonDetails extends StatefulWidget {
  const LessonDetails({Key? key, required this.currentLesson})
      : super(key: key);

  final Lesson currentLesson;
  @override
  _LessonDetailsState createState() => _LessonDetailsState();
}

class _LessonDetailsState extends State<LessonDetails> {
  @override
  Widget build(BuildContext context) {
    Lesson.loadInitLessons();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.currentLesson.title),
      ),
      body: Center(
        child: Column(
          children: [
            CircleAvatar(
                backgroundImage: NetworkImage(widget.currentLesson.imgUrl)),
            Text(
              widget.currentLesson.title,
              style: TextStyle(
                color: Colors.blue,
                fontSize: 35,
              ),
            ),
            Text(widget.currentLesson.subtitle,
                style: TextStyle(color: Colors.amber, fontSize: 21)),
            Icon(widget.currentLesson.icon)
          ],
        ),
      ),
    );
  }
}
