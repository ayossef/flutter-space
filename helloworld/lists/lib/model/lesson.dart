import 'package:flutter/material.dart';

class Lesson {
  String title;
  String subtitle;
  IconData icon;
  String imgUrl =
      "https://res.cloudinary.com/startup-grind/image/upload/dpr_2.0,fl_sanitize/v1/gcs/platform-data-goog/contentbuilder/logo_dark_QmPdj9K.svg";

  static List<Lesson> lessonsList = [];
  Lesson(this.title, this.icon, this.subtitle);
  static loadInitLessons() {
    lessonsList.add(Lesson("Day One Title", Icons.access_alarms, "First Day"));
    lessonsList.add(Lesson("Day Two", Icons.add_to_drive_rounded, "2nd Day"));
    lessonsList.add(Lesson(
        "Day Three", Icons.airline_seat_legroom_reduced_outlined, "3rd Day"));
    lessonsList.add(Lesson("Day Four", Icons.airplay, "4th Day"));
  }
}
