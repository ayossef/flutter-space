import 'package:flutter/material.dart';

class Countries extends StatefulWidget {
  const Countries({Key? key}) : super(key: key);

  @override
  _CountriesState createState() => _CountriesState();
}

class _CountriesState extends State<Countries> {
  List<String> countries = ["MZ", "EG", "UK", "US", "ZA"];
  String _country = "MZ";
  int _currentIndex = 0;
  void incIndex() {
    if (_currentIndex == countries.length - 1) {
      setState(() {
        _currentIndex = 0;
      });
    } else {
      setState(() {
        _currentIndex++;
      });
    }
  }

  void decrementIndex() {
    if (_currentIndex == 0) {
      setState(() {
        _currentIndex = countries.length - 1;
      });
    } else {
      setState(() {
        _currentIndex = _currentIndex - 1;
      });
    }
  }

  void updateCountry() {
    setState(() {
      _country = countries[_currentIndex];
    });
  }

  void showNextCountry() {
    incIndex();
    updateCountry();
  }

  void showPreCountry() {
    decrementIndex();
    updateCountry();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(),
            MaterialButton(
              onPressed: () => {},
              child: Text("Add to list"),
            ),
            Text("$_country"),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                  onPressed: showPreCountry,
                  child: Text("Previous"),
                ),
                MaterialButton(
                  onPressed: showNextCountry,
                  child: Text("Next"),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
