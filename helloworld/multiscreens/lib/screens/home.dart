import 'package:flutter/material.dart';
import 'package:multiscreens/model/user.dart';
import 'package:multiscreens/screens/next.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key, required this.user}) : super(key: key);

  final User user;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.name),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Name: ",
                      style: TextStyle(backgroundColor: Colors.amber),
                    ),
                  ),
                  Text(
                    widget.user.name,
                    style: TextStyle(
                      backgroundColor: Colors.amber,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.user.email ?? "-----",
                style: TextStyle(backgroundColor: Colors.amber),
              ),
            ),
            Text(
              widget.user.email == null ? "-----" : widget.user.email!,
              style: TextStyle(backgroundColor: Colors.amber),
            ),
            Text(
              widget.user.email == null ? "Email is null" : "Email exists",
              style: TextStyle(backgroundColor: Colors.amber),
            ),
            Text(
              "${widget.user.id}",
              style: TextStyle(backgroundColor: Colors.amber),
            ),
            MaterialButton(
              onPressed: navigate,
              child: Row(
                children: [
                  Text("Next Screen"),
                  Icon(Icons.arrow_forward_ios_outlined)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  navigate() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => NextScreen()));
  }
}
