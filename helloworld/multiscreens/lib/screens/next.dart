import 'package:flutter/material.dart';

class NextScreen extends StatefulWidget {
  const NextScreen({Key? key}) : super(key: key);

  @override
  _NextScreenState createState() => _NextScreenState();
}

class _NextScreenState extends State<NextScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Next Screen"),
      ),
      body: Center(
        child: GestureDetector(
          onSecondaryTap: () {
            print("Secondery Tapped");
          },
          onTap: () {
            print("Tapped");
          },
          onDoubleTap: () {
            print("doube Tapped");
          },
          onLongPress: () {
            print("Long Press");
          },
          onHorizontalDragStart: (startDetails) =>
              {print("Start: $startDetails")},
          onHorizontalDragEnd: (endDetails) => {print("End: $endDetails")},
          child: Column(
            children: [
              Text("Welcome to next Screen"),
              MaterialButton(
                onPressed: () => {Navigator.pop(context)},
                child: Row(
                  children: [Text("Home Screen"), Icon(Icons.home_filled)],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
