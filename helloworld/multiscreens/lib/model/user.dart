class User {
  String name;
  String? email;
  int id;

  User({required this.id, required this.name, this.email});
}
