import 'package:flutter/material.dart';
import 'package:mycv/widgets/addcertificate.dart';
import 'package:mycv/widgets/cerificateslist.dart';

class Certificates extends StatefulWidget {
  const Certificates({super.key});

  @override
  State<Certificates> createState() => _CertificatesState();
}

class _CertificatesState extends State<Certificates> {
  int _selectedIndex = 0;
  List<Widget> _views = [CertificatesList(), const AddCertificate()];
  @override
  Widget build(BuildContext context) {
    initState() {
      _selectedIndex = 0;
    }

    void _updateView(int value) {
      print('Selected Value $value');
      setState(() {
        _selectedIndex = value;
        print(_selectedIndex);
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Certifications'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: _views[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.list), label: 'List'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Add New')
        ],
        onTap: (value) => {_updateView(value)},
      ),
    );
  }
}
