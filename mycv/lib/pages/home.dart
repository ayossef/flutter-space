import 'package:flutter/material.dart';
import 'package:mycv/model/navmanager.dart';
import 'package:mycv/pages/certificates.dart';
import 'package:mycv/widgets/contactform.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectIndex = 0;
  void _navigateToCertificates(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => Certificates()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Home',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Home Page'),
            MaterialButton(
              onPressed: () {
                _navigateToCertificates(context);
              },
              child: Text('My Certifications'),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: NavigationManager().getNavBarItems(),
        onTap: (value) {
          print('User has selected value $value');
          setState(() {
            _selectIndex = value;
          });
        },
      ),
    );
  }
}
