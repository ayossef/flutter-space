import 'package:flutter/material.dart';

class NavigationManager {
  List<BottomNavigationBarItem> itemsList = [];
  List<Widget> views = [];
  NavigationManager() {
    BottomNavigationBarItem aboutme = const BottomNavigationBarItem(
        icon: Icon(
          Icons.usb_off_rounded,
          color: Colors.deepPurple,
        ),
        label: 'About Me');
    itemsList.add(aboutme);

    BottomNavigationBarItem education = const BottomNavigationBarItem(
        icon: Icon(
          Icons.book,
          color: Colors.deepPurple,
        ),
        label: 'Education');
    itemsList.add(education);

    BottomNavigationBarItem work = const BottomNavigationBarItem(
        icon: Icon(
          Icons.work_off_outlined,
          color: Colors.deepPurple,
        ),
        label: 'Work');
    itemsList.add(work);

    BottomNavigationBarItem contacts = const BottomNavigationBarItem(
        icon: Icon(
          Icons.contacts_sharp,
          color: Colors.deepPurple,
        ),
        label: 'Conact Me');
    itemsList.add(contacts);
  }

  List<BottomNavigationBarItem> getNavBarItems() {
    print('List is created with $itemsList');
    return itemsList;
  }
}
