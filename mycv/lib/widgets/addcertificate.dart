import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mycv/model/certificatesmanager.dart';

class AddCertificate extends StatefulWidget {
  const AddCertificate({super.key});

  @override
  State<AddCertificate> createState() => _AddCertificateState();
}

class _AddCertificateState extends State<AddCertificate> {
  TextEditingController _certNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          TextField(
            controller: _certNameController,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.amber,
              fontSize: 24,
            ),
          ),
          Container(
              margin: EdgeInsets.all(5),
              child: ElevatedButton(
                  onPressed: () {}, child: Text('Add To the List')))
        ],
      ),
    );
  }
}
