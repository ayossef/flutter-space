import 'package:flutter/material.dart';

class CertificatesList extends StatelessWidget {
  CertificatesList({super.key});
  List<String> certs = ['CISSP', 'CISM'];
  @override
  Widget build(BuildContext context) {
    return Center(
        child: ListView.builder(
      itemBuilder: (context, index) => Text(certs[index]),
      itemCount: 2,
    ));
  }
}
