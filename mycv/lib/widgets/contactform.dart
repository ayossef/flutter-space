import 'package:flutter/material.dart';

class ContactsForm extends StatefulWidget {
  const ContactsForm({super.key});

  @override
  State<ContactsForm> createState() => _ContactsFormState();
}

class _ContactsFormState extends State<ContactsForm> {
  final TextEditingController _emailController = TextEditingController();
  void _sendAction() {
    print(_emailController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        TextField(
          keyboardType: TextInputType.emailAddress,
          textAlign: TextAlign.center,
          autofocus: true,
          style: const TextStyle(fontSize: 24),
          controller: _emailController,
        ),
        const TextField(
          keyboardType: TextInputType.emailAddress,
          textAlign: TextAlign.center,
          autofocus: true,
          style: const TextStyle(fontSize: 24),
        ),
        MaterialButton(onPressed: _sendAction),
        Expanded(
            child: ListView.builder(
                itemBuilder: ((context, index) => Text('Value')))),
      ]),
    );
  }
}
